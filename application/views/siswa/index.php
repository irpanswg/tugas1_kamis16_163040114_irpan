<html>
	<head>
		<title>Toko Mainan</title>
	</head>
	<body>
		<h1>Daftar Mainan</h1>
		<hr>

		<a href='<?php echo base_url("siswa/tambah"); ?>'>Tambah Data</a><br><br>

		<table border="1" cellpadding="7">
			<tr>
				<th>Kode Mainan</th>
				<th>Nama</th>
				<th>Harga</th>
				<th>Stock</th>
				<th>Bahan</th>
				<th colspan="2">Aksi</th>
			</tr>

			<?php
			if( ! empty($mainan)){ // Jika data siswa tidak sama dengan kosong, artinya jika data siswa ada
				foreach($mainan as $data){
					echo "<tr>
					<td>".$data->kode."</td>
					<td>".$data->nama."</td>
					<td>".$data->harga."</td>
					<td>".$data->stock."</td>
					<td>".$data->bahan."</td>
					<td><a href='".base_url("siswa/ubah/".$data->kode)."'>Ubah</a></td>
					<td><a href='".base_url("siswa/hapus/".$data->kode)."'>Hapus</a></td>
					</tr>";
				}
			}else{ // Jika data siswa kosong
				echo "<tr><td align='center' colspan='7'>Data Tidak Ada</td></tr>";
			}
			?>
		</table>
	</body>
</html>
