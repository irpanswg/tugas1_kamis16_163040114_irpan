<html>
	<head>
		<title>Form Ubah Mainan</title>
	</head>
	<body>
		<h1>Form Ubah Data Mainan</h1>
		<hr>

		<!-- Menampilkan Error jika validasi tidak valid -->
		<div style="color: red;"><?php echo validation_errors(); ?></div>

		<?php echo form_open("siswa/ubah/".$mainan->kode); ?>
			<table cellpadding="8">
				<tr>
					<td>Kode Mainan</td>
					<td><input type="text" name="input_kode" value="<?php echo set_value('input_kode', $mainan->kode); ?>" readonly></td>
				</tr>
				<tr>
					<td>Nama</td>
					<td><input type="text" name="input_nama" value="<?php echo set_value('input_nama', $mainan->nama); ?>"></td>
				</tr>
				<tr>
					<td>Harga</td>
					<td><input type="text" name="input_harga" value="<?php echo set_value('input_harga', $mainan->harga); ?>" readonly></td>
				</tr>
				<tr>
					<td>Stock</td>
					<td><input type="text" name="input_stock" value="<?php echo set_value('input_stock', $mainan->stock); ?>" readonly></td>
				</tr>
				<tr>
					<td>Bahan</td>
					<td><input type="text" name="input_bahan" value="<?php echo set_value('input_bahan', $mainan->bahan); ?>" readonly></td>
				</tr>
				
			</table>
				
			<hr>
			<input type="submit" name="submit" value="Ubah">
			<a href="<?php echo base_url(); ?>"><input type="button" value="Batal"></a>
		<?php echo form_close(); ?>
	</body>
</html>
