<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SiswaModel extends CI_Model {
	// Fungsi untuk menampilkan semua data siswa
	public function view(){
		return $this->db->get('mainan')->result();
	}
	
	// Fungsi untuk menampilkan data siswa berdasarkan NIS nya
	public function view_by($kode){
		$this->db->where('kode', $kode);
		return $this->db->get('mainan')->row();
	}
	
	// Fungsi untuk validasi form tambah dan ubah
	public function validation($mode){
		$this->load->library('form_validation'); // Load library form_validation untuk proses validasinya
		
		// Tambahkan if apakah $mode save atau update
		// Karena ketika update, NIS tidak harus divalidasi
		// Jadi NIS di validasi hanya ketika menambah data siswa saja
		if($mode == "save")
			$this->form_validation->set_rules('input_kode', 'kode', 'required|numeric|max_length[10]');
		
		$this->form_validation->set_rules('input_nama', 'nama', 'required|max_length[25]');
		$this->form_validation->set_rules('input_harga', 'harga', 'required|numeric|max_length[20]');
		$this->form_validation->set_rules('input_stock', 'stock', 'required|numeric|max_length[20]');
		$this->form_validation->set_rules('input_bahan', 'bahan', 'required|max_length[25]');
		
			
		if($this->form_validation->run()) // Jika validasi benar
			return TRUE; // Maka kembalikan hasilnya dengan TRUE
		else // Jika ada data yang tidak sesuai validasi
			return FALSE; // Maka kembalikan hasilnya dengan FALSE
	}
	
	// Fungsi untuk melakukan simpan data ke tabel siswa
	public function save(){
		$data = array(
			"kode" => $this->input->post('input_kode'),
			"nama" => $this->input->post('input_nama'),
			"harga" => $this->input->post('input_harga'),
			"stock" => $this->input->post('input_stock'),
			"bahan" => $this->input->post('input_bahan')
		);
		
		$this->db->insert('mainan', $data); // Untuk mengeksekusi perintah insert data
	}
	
	// Fungsi untuk melakukan ubah data siswa berdasarkan NIS siswa
	public function edit($kode){
		$data = array(
			"nama" => $this->input->post('input_nama'),
			"harga" => $this->input->post('input_harga'),
			"stock" => $this->input->post('input_stock'),
			"bahan" => $this->input->post('input_bahan')
		);
		
		$this->db->where('kode', $kode);
		$this->db->update('mainan', $data); // Untuk mengeksekusi perintah update data
	}
	
	// Fungsi untuk melakukan menghapus data siswa berdasarkan NIS siswa
	public function delete($kode){
		$this->db->where('kode', $kode);
		$this->db->delete('mainan'); // Untuk mengeksekusi perintah delete data
	}
}
