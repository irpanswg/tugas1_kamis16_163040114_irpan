-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 24 Okt 2018 pada 17.59
-- Versi server: 10.1.36-MariaDB
-- Versi PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `phpmvc_mainan`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `mainan`
--

CREATE TABLE `mainan` (
  `kode` int(10) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `harga` int(20) NOT NULL,
  `stock` int(20) NOT NULL,
  `bahan` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mainan`
--

INSERT INTO `mainan` (`kode`, `nama`, `harga`, `stock`, `bahan`) VALUES
(1, 'barbie', 50000, 15, 'plastik');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `mainan`
--
ALTER TABLE `mainan`
  ADD PRIMARY KEY (`kode`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
